from django.shortcuts import render
from django.core.mail import send_mail
from django.template import loader
from django.http import HttpResponse
from blogtest.models import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from datetime import datetime
# Create your views here.
def robot(request):
    arbs=Blog.objects.all()
    t= loader.get_template("robots.txt")
    c= {'arbs':arbs}
    return HttpResponse(t.render(c))
#

#@login_required
def home(request,tmpl='index.html'):
    """
    общедоступная домашняя страница
    """
    news=Blog.objects.all()
    t= loader.get_template(tmpl)
    wus = request.user
    title='Полный архив для '
    new='new'
    c= {'title':title,'new':new,'news':news,'wus':wus}
    return HttpResponse(t.render(c))
#
@login_required
def list_rss(request,allread=False,tmpl='index.html'):
    """
    вывод списков постов подписантов
    """
    t= loader.get_template(tmpl)
    wus = request.user
    news=LineRSS.objects.filter(user=wus).order_by('-new', '-timestamp')
    if allread:
        news.update(new=False)
    title='Лента новостей для '
    new='old'
    c= {'title':title,'new':new,'news':news,'wus':wus}
    return HttpResponse(t.render(c))
#
@login_required
def repo(request,tmpl='form_rss.html',auth='o',do='re'):
    """
    добавление подписки -- po
    удаление -- re
    """
    wus = request.user

    b=UserLinkAuthor()
    b.user=wus
    b.author=auth
    if do=='po': # подписаться
        bs=UserLinkAuthor.objects.filter(user=wus)
        bsb=bs.filter(author=auth)
        bss=bsb.filter(done=True)
        b.done=True
        um=User.objects.get(username=wus).email
        b.usermail=um
        b.save()
        ba=Blog.objects.filter(author=auth)
        baa=ba.values('pk')
        bac=ba.values('caption')
        bat=ba.values('timestamp')
        j=0
        for i in baa:
            tmp=LineRSS()
            tmp.caption=bac[j]['caption']
            tmp.author=auth
            tmp.timestamp=bat[j]['timestamp']
            tmp.new=True
            tmp.user=wus
            tmp.save()
            j+=1

    if do=='re':
        bsb=UserLinkAuthor.objects.filter(user=wus)
        b=bsb.filter(author=auth)
        b.delete()
        dl=LineRSS.objects.filter(user=wus).filter(author=auth).delete()
    t= loader.get_template(tmpl)
    aus=UserLinkAuthor.objects.filter(user=wus)
    nws=Blog.objects.all().order_by('author').values('author').distinct()
    news=[]
    for i in nws:
        au=i['author']
        news.append(au)


    c= {'news':news,'aus':aus,'wus':wus}
    return HttpResponse(t.render(c))
#
def blog_find(request):
    """
    поиск по постам и заголовкам
    """
    try:
        new=request.GET['new']
    except:
        new=''
    key=request.GET['key']
    key=key.replace("'","")
    key=key.replace('"',"")
    aas=Blog.objects.all()
    ar=aas.filter(memo__icontains=key)
    arr=aas.filter(caption__icontains=key)
    try:
        arbs=ar.union(arr)
    except KeyError:
        arbs=arb

    if new=='old':
        id=request.GET['id']
        a=LineRSS.objects.get(id=id).pk
        c=LineRSS.objects.get(id=id).caption
        u=LineRSS.objects.get(id=id).user
        t=LineRSS.objects.get(id=id).timestamp
        b=LineRSS()
        b.new=False
        b.pk=a
        b.caption=c
        b.user=u
        b.timestamp=t
        b.save()
    t=loader.get_template('index.html')
    c={'news':arbs,'wus':request.user}
    return HttpResponse(t.render(c))
#

@login_required
def save_form(request):
    """
    сохранение поста
    """
    caption=request.GET['caption']
    memo=request.GET['memo']
    author=request.user
    g=Blog()
    g.caption=caption
    g.memo=memo
    g.author=author
#    print(g.pk)
#    mailtxt=g.name+' + '+g.mail+' -- '+g.memo
#    try:
#        gt=Guest.objects.get(memo=g.memo)
#    except Guest.DoesNotExist:
    g.save()
#    except Guest.MultipleObjectsReturned:
#        pass
    err='no error'
#    try:
#    send_mail('задан вопрос',mailtxt,'django@sens',['olab@email.su','alexper1949@mail.ru'],fail_silently=True)
#    except SMTPRecipientsRefused:
##        err='Почтовый адрес получателя не существует'
#    except SMTPSenderRefused:
#        err='Постовый адрес отправителя не существует'
#    except SMTPConnectError:
#        err='Не удалось установить соединение с почтовым сервером'
#    except SMTPAuthenticationError:
#        err='Неверные имя пользователя или пароль'
#    except SMTPServerDisconnected:
#        err='Соединение с сервером было внезапно разорвано'
#    except SMTPException:
#        err='Возникла иная неполадка'
    print(err)
    cs=Blog.objects.all() #.extra(where=['id = %s'],params=[ide])
    t=loader.get_template('posted.html')
    wus = request.user
    c={'notes':cs,'wus':wus}
    return HttpResponse(t.render(c))

