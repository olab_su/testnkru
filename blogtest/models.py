from django.db import models
from django.contrib import admin
from django.core.mail import send_mail
from test_nkru import settings
# Create your models here.
class UserLinkAuthor(models.Model):
    """
    таблица подписчиков
    """
    author=models.CharField(max_length=100)
    user=models.CharField(max_length=100)
    usermail=models.EmailField(default='olab@email.su')
    done=models.BooleanField(blank=True)
    timestamp=models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.author
    class Meta:
        ordering=('-timestamp','user','author',)
class LineRSS(models.Model):
    """
    таблица подписки
    """
    user=models.CharField(max_length=100)
    new=models.BooleanField(blank=True)
    timestamp=models.DateTimeField(auto_now_add=True)
    caption=models.CharField(default='-',max_length=150,verbose_name='Заголовок ')
    author=models.CharField(default='o',max_length=100,verbose_name='Автор ')
    def __str__(self):
        return self.user
    class Meta:
        ordering=('-timestamp','user',)
class Blog(models.Model):
    """
    основная таблица
    """
    caption=models.CharField(max_length=150,verbose_name='Заголовок ')
    author=models.CharField(max_length=100,verbose_name='Автор ')
    memo=models.TextField()
    timestamp=models.DateTimeField(auto_now_add=True)
    def save(self,*args,**kwargs):
        """
        переопределяем для отправки почты
        и создания лекты новостей
        """
        super(Blog,self).save(*args,**kwargs)
        u=UserLinkAuthor.objects.filter(author=self.author).values('user','usermail').distinct()
        users_mess=[]
        for i in u:
            au=i['user']
            lr=LineRSS()
            lr.user=au
            lr.new=True
            lr.caption=self.caption
            lr.author=self.author
            lr.save()
            usp=i['usermail']
            users_mess.append(usp)
        aut=self.author
        li='<a href="http://{0}/key/?key={1}"> {1} </a>'.format(settings.HOST_URL,self.caption)
        mssg='new post {0} by authors {1}'.format(li,aut)
        msg='new post {0} by authors {1}'.format(self.caption,aut)
        send_mail('Information',msg,settings.EMAIL_HOST_USER,users_mess,html_message=mssg)
    def __str__(self):
       return self.caption
    class Meta:
        ordering=('-timestamp',)
        verbose_name='Блог'
        verbose_name_plural='Блоги'
class LineRSSAdmin(admin.ModelAdmin):
    list_display=('user','author','caption','timestamp')
#
class BlogAdmin(admin.ModelAdmin):
    list_display=('caption','timestamp','author')
class UserLinkAuthorAdmin(admin.ModelAdmin):
    list_display=('user','author')
#
admin.site.register(LineRSS,LineRSSAdmin)
admin.site.register(Blog,BlogAdmin)
admin.site.register(UserLinkAuthor,UserLinkAuthorAdmin)
#
